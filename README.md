This project was modified from https://gitlab.com/kenzie-academy/misc/init-gitlab-page

# How to Install

On your terminal, run `npm install --global git+https://gitlab.com/crmartinez239/init-react-page.git`. You may need to restart your terminal (or source your bash profile) before you can use it.

# How to Use

On your terminal, in your git project folder, run `init-react-page`. This will generate a GitLab CI YAML file with the correct settings to tell GitLab to host a React app.
