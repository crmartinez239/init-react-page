// original source can be found at
// https://gitlab.com/kenzie-academy/misc/init-gitlab-page

module.exports = (items, indentationLevel = 2) => 
  items.reduce(
    (collection, item) => {
      const indentation = "- ".padStart(indentationLevel * 2 + 2)
      return `${collection}\n${indentation}${item}`
    },
    ''
  )
