// original source can be found at
// https://gitlab.com/kenzie-academy/misc/init-gitlab-page

const createYAMLCollection = require('./create-yaml-collection')

module.exports = publicExists => {
  const shellCommands = {
    ifPublicExists: [
      `npm install`,
      `npm run build`,
      `rm -rf public`,
      `mv build public`
    ],

    ifPublicDoesNotExist: [
      `# I don't know what to do here`
    ]
  }

  const deploymentShellCommands = publicExists
    ? createYAMLCollection(shellCommands.ifPublicExists)
    : createYAMLCollection(shellCommands.ifPublicDoesNotExist)

  return (
`image: node:alpine

pages:
  stage: deploy
  script: ${deploymentShellCommands}
  artifacts:
    paths:
      - public 
  only:
    - master`
  )
}
